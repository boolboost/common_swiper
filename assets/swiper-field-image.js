(function () {
  'use strict';

  let container = document.querySelectorAll('.swiper-container');

  let defaultPagination = {
    el: '.swiper-pagination',
    clickable: true,
  };

  container.forEach(function (el) {
    let style = getComputedStyle(el);

    let slidesPerView = style.getPropertyValue('--swiper-slides-per-view') || 1;
    let spaceBetween = parseInt(style.getPropertyValue('--swiper-space-between') || 0);
    let pagination = style.getPropertyValue('--swiper-pagination') === '1' ? defaultPagination : {};

    let swiper = new Swiper(el, {
      loop: true,
      slidesPerView: slidesPerView,
      spaceBetween: spaceBetween,
      pagination: pagination,

      navigation: {
        nextEl: '.swiper-button-next',
        prevEl: '.swiper-button-prev'
      },

      // And if we need scrollbar
      scrollbar: {
        el: '.swiper-scrollbar'
      },
      on: {
        resize: function () {
          let style = getComputedStyle(el);

          let slidesPerView = style.getPropertyValue('--swiper-slides-per-view') || 1;
          let spaceBetween = parseInt(style.getPropertyValue('--swiper-space-between') || 0);
          let pagination = style.getPropertyValue('--swiper-pagination') === '1' ? defaultPagination : {};

          if (slidesPerView !== swiper.params.slidesPerView) {
            swiper.params.slidesPerView = slidesPerView;
          }

          if (spaceBetween !== swiper.params.spaceBetween) {
            swiper.params.spaceBetween = spaceBetween;
          }

          if (pagination !== swiper.params.pagination) {
            swiper.params.pagination = pagination;
          }
        }
      }
    });
  });

})();
