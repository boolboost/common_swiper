<?php

namespace Drupal\common_swiper\Plugin\Field\FieldFormatter;

use Drupal\entity_reference_revisions\Plugin\Field\FieldFormatter\EntityReferenceRevisionsEntityFormatter;

/**
 * Plugin implementation of the 'Swiper' formatter.
 *
 * @FieldFormatter(
 *   id = "swiper_entity_reference_revisions",
 *   label = @Translation("Swiper"),
 *   field_types = {
 *     "entity_reference_revisions"
 *   }
 * )
 */
class SwiperEntityReferenceRevisionsFieldFormatter extends EntityReferenceRevisionsEntityFormatter {

  use SwiperFieldFormatterTrait;
}
