<?php

namespace Drupal\common_swiper\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatter;

/**
 * Plugin implementation of the 'Swiper' formatter.
 *
 * @FieldFormatter(
 *   id = "swiper_image",
 *   label = @Translation("Swiper"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class SwiperImageFieldFormatter extends ImageFormatter {

  use SwiperFieldFormatterTrait;
}
