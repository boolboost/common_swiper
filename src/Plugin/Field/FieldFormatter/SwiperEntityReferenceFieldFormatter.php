<?php

namespace Drupal\common_swiper\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\Plugin\Field\FieldFormatter\EntityReferenceEntityFormatter;

/**
 * Plugin implementation of the 'Swiper' formatter.
 *
 * @FieldFormatter(
 *   id = "swiper_entity_reference",
 *   label = @Translation("Swiper"),
 *   field_types = {
 *     "entity_reference"
 *   }
 * )
 */
class SwiperEntityReferenceFieldFormatter extends EntityReferenceEntityFormatter {

  use SwiperFieldFormatterTrait;
}
