<?php

namespace Drupal\common_swiper\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FieldItemListInterface;

trait SwiperFieldFormatterTrait {

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    return [$this->t('Displays Swiper Carousel.')];
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = parent::viewElements($items, $langcode);

    $elements['#theme'] = 'swiper';
    $elements['#items'] = [];

    foreach ($elements as $key => $item) {
      if (\is_numeric($key)) {
        $elements['#items'][$key] = $item;
      }
    }

    return $elements;
  }

}
