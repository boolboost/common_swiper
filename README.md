## Install

Add to composer.json.

``` json
{
    "require": {
        "nolimits4web/swiper": "4.3.0"
    },
    "repositories": [
        {
            "type": "package",
            "package": {
                "name": "nolimits4web/swiper",
                "type": "drupal-library",
                "version": "4.3.0",
                "dist": {
                    "url": "https://github.com/nolimits4web/swiper/archive/v4.3.0.zip",
                    "type": "zip"
                }
            }
        }
    ]
}
```

## Example

``` php
$carousel = [
  '#theme' => 'swiper',
  '#items' => $items,
  // Default Settings.
  'attributes' => ['class' => ['swiper-container']],
  'wrapper_attributes' => ['class' => ['swiper-wrapper']],
  'slide_attributes' => ['class' => ['swiper-slide']],
  'settings' => [
    'pagination' => FALSE,
    'button' => TRUE,
    'scrollbar' => FALSE,
  ],
];
```

## Example for theme Glisseo

``` php
/**
 * Implements hook_preprocess_HOOK() for swiper.html.twig.
 */
function hook_preprocess_swiper(&$variables) {
  if (isset($variables['entity_type'], $variables['field_name'])) {
    // Base class theme.
    $block = \_glisseo_bem("{$variables['entity_type']}-{$variables['field_name']}");
    $variables['attributes']['class'][] = $block;
    $variables['wrapper_attributes']['class'][] = \_glisseo_bem($block, 'items');
    $variables['slide_attributes']['class'][] = \_glisseo_bem($block, 'item');
  }

  if (
    $variables['entity_type'] == 'block_content' &&
    $variables['bundle'] == 'our_clients_and_partners' &&
    $variables['field_name'] == 'field_logos'
  ) {
    // 2 elements in slide.
    $variables['items'] = \array_chunk($variables['items'], 2);
  }
}

```
